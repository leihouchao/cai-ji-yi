#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include "ui_dialog.h"

extern unsigned short ctrl;
#include <QDebug>

/*************** 初始化 ********************************/
/*
 * 入参:
 *   设备ID: devIDH,devIDL
 *   采样频率: fre
 *   服务器发送间隔: sendTimeServer
 *   触发态发送间隔: sendTimeDynamic
 *   采样类型: ctrL,ctrH
*/
Dialog::Dialog(QWidget *parent, unsigned char devIDH, unsigned char devIDL, unsigned char ctrlH, unsigned char ctrlL,\
               unsigned char fre, unsigned char sendTimeServer, unsigned char sendTimeDynamic,QString _proId,QString _proName,\
               QString _testPointName[16]) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    // 1,窗口设置
    ui->setupUi(this);
    this->setWindowTitle("参数设置");
    pMainWindow = static_cast<MainWindow *>(parent);
    pMainWindow->readInitialValue = true;
    for(int i = 0;i<20;i++) isSelected[i] = false;

    /******************** (1),设备上传的配置 **************/
    // 1,设备ID
    int devid = 0;
    devid = devIDH;
    devid <<= 8;
    devid += devIDL;
    QRegExp regExp("[0-9]{0,5}");
    ui->lineEdit_DevID->setValidator(new QRegExpValidator(regExp,this));
    this->ui->lineEdit_DevID->setText(QString::number(devid));
    pDevIDH = devIDH;
    pDevIDL = devIDL;
    // 2,设置采样频率
    ui->comboBox_SampleRte->setCurrentText(QString::number(fre));
    pFre = fre;
    // 3,设置服务器发送间隔
    ui->comboBox_SendTime2erver->setCurrentText(QString::number(sendTimeServer));
    pSendTimeServer = sendTimeServer;
    // 4,设置触发态发送间隔
    ui->comboBox_SendTimeDynamic->setCurrentText(QString::number(sendTimeDynamic));
    pSendTimeDynamic = sendTimeDynamic;
    // 5,设置采样类型
    if(ctrlL&0x01){
        ui->comboBox->setCurrentText("电压");
    }else{
        ui->comboBox->setCurrentText("电流");
    }
    if(ctrlL&0x02){
        ui->comboBox_2->setCurrentText("电压");
    }else{
        ui->comboBox_2->setCurrentText("电流");
    }
    if(ctrlL&0x04){
        ui->comboBox_3->setCurrentText("电压");
    }else{
        ui->comboBox_3->setCurrentText("电流");
    }
    if(ctrlL&0x08){
        ui->comboBox_4->setCurrentText("电压");
    }else{
        ui->comboBox_4->setCurrentText("电流");
    }
    if(ctrlL&0x10)
    {
        ui->comboBox_5->setCurrentText("电压");
    }else{
        ui->comboBox_5->setCurrentText("电流");
    }
    if(ctrlL&0x20){
        ui->comboBox_6->setCurrentText("电压");
    }else{
        ui->comboBox_6->setCurrentText("电流");
    }
    if(ctrlL&0x40){
        ui->comboBox_7->setCurrentText("电压");
    }else{
        ui->comboBox_7->setCurrentText("电流");
    }
    if(ctrlL&0x80){
        ui->comboBox_8->setCurrentText("电压");
    }else{
        ui->comboBox_8->setCurrentText("电流");
    }
    if(ctrlH&0x01){
        ui->comboBox_9->setCurrentText("电压");
    }else{
        ui->comboBox_9->setCurrentText("电流");
    }
    if(ctrlH&0x02){
        ui->comboBox_10->setCurrentText("电压");
    }else{
        ui->comboBox_10->setCurrentText("电流");
    }
    if(ctrlH&0x04){
        ui->comboBox_11->setCurrentText("电压");
    }else{
        ui->comboBox_11->setCurrentText("电流");
    }
    if(ctrlH&0x08){
        ui->comboBox_12->setCurrentText("电压");
    }else{
        ui->comboBox_12->setCurrentText("电流");
    }
    if(ctrlH&0x10){
        ui->comboBox_13->setCurrentText("电压");
    }else{
        ui->comboBox_13->setCurrentText("电流");
    }
    if(ctrlH&0x20){
        ui->comboBox_14->setCurrentText("电压");
    }else{
        ui->comboBox_14->setCurrentText("电流");
    }
    if(ctrlH&0x40){
        ui->comboBox_15->setCurrentText("电压");
    } else{
        ui->comboBox_15->setCurrentText("电流");
    }
    if(ctrlH&0x80){
        ui->comboBox_16->setCurrentText("电压");
    }
    else{
        ui->comboBox_16->setCurrentText("电流");
    }
    pCtrlL = ctrlL;
    pCtrlH = ctrlH;
    // 6,设置工程流水号
    proId = _proId;
    ui->lineEdit_projectID->setText(proId);
    // 7,设置项目名称
    proName = _proName;
    ui->lineEdit_projectName->setText(proName);
    // 8,设置测试点的名称
    for(int i = 0;i<16;i++){
        testPointName[i] = _testPointName[i];
    }
    ui->lineEdit_test1->setText(testPointName[0]);
    ui->lineEdit_test2->setText(testPointName[1]);
    ui->lineEdit_test3->setText(testPointName[2]);
    ui->lineEdit_test4->setText(testPointName[3]);
    ui->lineEdit_test5->setText(testPointName[4]);
    ui->lineEdit_test6->setText(testPointName[5]);
    ui->lineEdit_test7->setText(testPointName[6]);
    ui->lineEdit_test8->setText(testPointName[7]);

    ui->lineEdit_test9->setText(testPointName[8]);
    ui->lineEdit_test10->setText(testPointName[9]);
    ui->lineEdit_test11->setText(testPointName[10]);
    ui->lineEdit_test12->setText(testPointName[11]);
    ui->lineEdit_test13->setText(testPointName[12]);
    ui->lineEdit_test14->setText(testPointName[13]);
    ui->lineEdit_test15->setText(testPointName[14]);
    ui->lineEdit_test16->setText(testPointName[15]);

     /******************** (2),setting.csv文件保存的配置 **************/
    QFile mfile("./Setting.csv");
    // 有配置文件,则读取配置以设置
    if(mfile.exists()){
        if(!mfile.isOpen()) mfile.open( QIODevice::ReadWrite  |QIODevice::Text);
        QTextStream steam(&mfile);
        // 1,设置服务器地址
        this->ui->lineEdit_serverAddr->setPlaceholderText(tr("域名或 IP 地址: 端口"));
        // 判断是否是同一台设备
        QString h = steam.readLine();
        qDebug()<<"(dialog.cpp)设备号:"<<h;
        int deviceId = h.toInt();
        // 如果是同一台设备,则读取文件配置以设置
        if(deviceId == devid ){
            qDebug()<<"(dialog.cpp)从 'setting.csv' 配置文件读取的设置是: "<<endl;
            // 2,设置自动自动保存间隔
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)自动保存间隔:"<<h;
            ui->comboBox_SaveDataDynamic->setCurrentText(h);
            on_comboBox_SaveDataDynamic_activated(h);     // 根据保存类型来判断是否允许选择自动保存路径

            // 3,设置自动保存路径
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)保存路径:"<<h;
            this->ui->lineEdit_file4save->setText(h);

            // 4,设置密度
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)密度:"<<h;
            this->ui->lineEdit_Density->setText(h);

            // 5,设置标定系数
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)标定系数:"<<h;
            this->ui->lineEdit_biaoding->setText(h);

            // 6,7,设置压强
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)Pmax:"<<h;
            this->ui->lineEdit_Pmax->setText(h);
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)Pmin:"<<h;
            this->ui->lineEdit_Pmin->setText(h);

            // 8,设置通道数
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)通道数:"<<h;
            QStringList list = h.split(",");
            bool tmp[16] = {false};
            for(int i = 0;i<16;i++){
                if(list.at(i).toInt() == 0){
                    tmp[i] = false;
                }
                else{
                    tmp[i] = true;
                }
            }
            help_setChannel(tmp);

            // 9,设置H初值
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)H初值:"<<h;
            help_setH(h);

            // 10,设置基准点
            h = steam.readLine();
            qDebug()<<"(dialog.cpp)基准点:"<<h;
            help_setBase(h);

        }
        // 如果不是同一台设备,则设置为默认设置
        else{
            // 2,设置自动自动保存间隔
            ui->comboBox_SaveDataDynamic->setCurrentText(tr("手动保存"));
            on_comboBox_SaveDataDynamic_activated(tr("手动保存"));     // 根据保存类型来判断是否允许选择自动保存路径
            // 3,设置自动保存路径
            this->ui->lineEdit_file4save->setText(tr("./"));
            // 4,设置密度
            this->ui->lineEdit_Density->setText(tr("1"));
            // 5,设置标定系数
            this->ui->lineEdit_biaoding->setText(tr("1"));
            // 6,7,设置压强
            this->ui->lineEdit_Pmax->setText(tr("2000"));
            this->ui->lineEdit_Pmin->setText(tr("-2000"));
            // 8,设置通道数
            bool tmp[16] = {false};
            for(int i = 0;i<16;i++){
                    tmp[i] = false;
            }
            help_setChannel(tmp);
            // 9, 设置H初值
            // 默认为空
            // 10, 设置基准点
            // 默认为空
            last_baseValue = -1;
        }
    }
    // 没有配置文件,则设置为默认设置
    else{
        // 1,设置服务器地址
        this->ui->lineEdit_serverAddr->setPlaceholderText(tr("域名或 IP 地址: 端口"));
        // 2,设置自动自动保存间隔
        ui->comboBox_SaveDataDynamic->setCurrentText(tr("手动保存"));
        on_comboBox_SaveDataDynamic_activated(tr("手动保存"));     // 根据保存类型来判断是否允许选择自动保存路径
        // 3,设置自动保存路径
        this->ui->lineEdit_file4save->setText(tr("./"));
        // 4,设置密度
        this->ui->lineEdit_Density->setText(tr("1"));
        // 5,设置标定系数
        this->ui->lineEdit_biaoding->setText(tr("1"));
        // 6,7, 设置压强
        this->ui->lineEdit_Pmax->setText(tr("2000"));
        this->ui->lineEdit_Pmin->setText(tr("-2000"));
        // 8,设置通道数
        bool tmp[16] = {false};
        for(int i = 0;i<16;i++){
            tmp[i] = false;
        }
        help_setChannel(tmp);
        // 9,设置H初值
        // 默认为空
        // 10,设置基准点
        // 默认为空
        last_baseValue = -1;
    }

    // 绑定基准点选择事件
    connect(ui->checkBox_BaseValue1, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue2, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue3, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue4, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue5, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue6, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue7, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue8, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue9, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue10, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue11, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue12, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue13, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue14, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue15, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));
    connect(ui->checkBox_BaseValue16, SIGNAL(clicked()), this, SLOT(on_checkBox_BaseValue_clicked()));

     // 绑定通道数选择事件
     connect(ui->checkBox_1,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_2,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_3,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_4,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_5,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_6,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_7,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_8,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_9,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_10,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_11,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_12,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_13,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_14,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_15,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));
     connect(ui->checkBox_16,SIGNAL(clicked()),this,SLOT(on_checkBox_ChannelSelected_clicked()));

     // 定时1分钟读取数据
     oneMinuteTimer = new QTimer(this);
     connect(oneMinuteTimer,SIGNAL(timeout()),this,SLOT(handlerTimerOut()));
     msgBox = new QMessageBox(QMessageBox::Information, tr("采集分析软件"),"");
     cnt = 20;      //  20s to read the initial H value;
}
Dialog::~Dialog()
{
    delete ui;
}

/*************** 辅助函数 *********************************/
// 辅助函数 -- 设置通道数
void Dialog::help_setChannel(bool a[])
{
    for(int i = 0;i<16;i++){
        isSelected[i] = a[i];
    }
   if(a[0]) ui->checkBox_1->setChecked(true);
   else  ui->checkBox_1->setChecked(false);
   if(a[1]) ui->checkBox_2->setChecked(true);
   else  ui->checkBox_2->setChecked(false);
   if(a[2]) ui->checkBox_3->setChecked(true);
   else  ui->checkBox_3->setChecked(false);
   if(a[3]) ui->checkBox_4->setChecked(true);
   else  ui->checkBox_4->setChecked(false);

   if(a[4]) ui->checkBox_5->setChecked(true);
   else  ui->checkBox_5->setChecked(false);
   if(a[5]) ui->checkBox_6->setChecked(true);
   else  ui->checkBox_6->setChecked(false);
   if(a[6]) ui->checkBox_7->setChecked(true);
   else  ui->checkBox_7->setChecked(false);
   if(a[7]) ui->checkBox_8->setChecked(true);
   else  ui->checkBox_8->setChecked(false);

   if(a[8]) ui->checkBox_9->setChecked(true);
   else  ui->checkBox_9->setChecked(false);
   if(a[9]) ui->checkBox_10->setChecked(true);
   else  ui->checkBox_10->setChecked(false);
   if(a[10]) ui->checkBox_11->setChecked(true);
   else  ui->checkBox_11->setChecked(false);
   if(a[11]) ui->checkBox_12->setChecked(true);
   else  ui->checkBox_12->setChecked(false);

   if(a[12]) ui->checkBox_13->setChecked(true);
   else  ui->checkBox_13->setChecked(false);
   if(a[13]) ui->checkBox_14->setChecked(true);
   else  ui->checkBox_14->setChecked(false);
   if(a[14]) ui->checkBox_15->setChecked(true);
   else  ui->checkBox_15->setChecked(false);
   if(a[15]) ui->checkBox_16->setChecked(true);
   else  ui->checkBox_16->setChecked(false);
}
// 辅助函数 -- 设置测试点名称
void Dialog::help_setTestName(QString s)
{
    QStringList list = s.split(",");

    if(list.at(0) != "-") ui->lineEdit_test1->setText(list.at(0));
    else ui->lineEdit_test1->setText(tr(""));
    if(list.at(1) != "-") ui->lineEdit_test2->setText(list.at(1));
    else ui->lineEdit_test2->setText(tr(""));
    if(list.at(2) != "-") ui->lineEdit_test3->setText(list.at(2));
    else ui->lineEdit_test3->setText(tr(""));
    if(list.at(3) != "-") ui->lineEdit_test4->setText(list.at(3));
    else ui->lineEdit_test4->setText(tr(""));

    if(list.at(4) != "-") ui->lineEdit_test5->setText(list.at(4));
    else ui->lineEdit_test5->setText(tr(""));
    if(list.at(5) != "-") ui->lineEdit_test6->setText(list.at(5));
    else ui->lineEdit_test6->setText(tr(""));
    if(list.at(6) != "-") ui->lineEdit_test7->setText(list.at(6));
    else ui->lineEdit_test7->setText(tr(""));
    if(list.at(7) != "-") ui->lineEdit_test8->setText(list.at(7));
    else ui->lineEdit_test8->setText(tr(""));

    if(list.at(8) != "-") ui->lineEdit_test9->setText(list.at(8));
    else ui->lineEdit_test9->setText(tr(""));
    if(list.at(9) != "-") ui->lineEdit_test10->setText(list.at(9));
    else ui->lineEdit_test10->setText(tr(""));
    if(list.at(10) != "-") ui->lineEdit_test11->setText(list.at(10));
    else ui->lineEdit_test11->setText(tr(""));
    if(list.at(11) != "-") ui->lineEdit_test12->setText(list.at(11));
    else ui->lineEdit_test12->setText(tr(""));

    if(list.at(12) != "-") ui->lineEdit_test13->setText(list.at(12));
    else ui->lineEdit_test13->setText(tr(""));
    if(list.at(13) != "-") ui->lineEdit_test14->setText(list.at(13));
    else ui->lineEdit_test14->setText(tr(""));
    if(list.at(14) != "-") ui->lineEdit_test15->setText(list.at(14));
    else ui->lineEdit_test15->setText(tr(""));
    if(list.at(15) != "-") ui->lineEdit_test16->setText(list.at(15));
    else ui->lineEdit_test16->setText(tr(""));
}
// 辅助函数 -- 设置H初值
void Dialog::help_setH(QString s){
    QStringList list = s.split(",");

    if(list.at(0) != "-") ui->lineEdit_H1->setText(list.at(0));
    else ui->lineEdit_H1->setText(tr(""));
    if(list.at(1) != "-") ui->lineEdit_H2->setText(list.at(1));
    else ui->lineEdit_H2->setText(tr(""));
    if(list.at(2) != "-") ui->lineEdit_H3->setText(list.at(2));
    else ui->lineEdit_H3->setText(tr(""));
    if(list.at(3) != "-") ui->lineEdit_H4->setText(list.at(3));
    else ui->lineEdit_H4->setText(tr(""));

    if(list.at(4) != "-") ui->lineEdit_H5->setText(list.at(4));
    else ui->lineEdit_H5->setText(tr(""));
    if(list.at(5) != "-") ui->lineEdit_H6->setText(list.at(5));
    else ui->lineEdit_H6->setText(tr(""));
    if(list.at(6) != "-") ui->lineEdit_H7->setText(list.at(6));
    else ui->lineEdit_H7->setText(tr(""));
    if(list.at(7) != "-") ui->lineEdit_H8->setText(list.at(7));
    else ui->lineEdit_H8->setText(tr(""));

    if(list.at(8) != "-") ui->lineEdit_H9->setText(list.at(8));
    else ui->lineEdit_H9->setText(tr(""));
    if(list.at(9) != "-") ui->lineEdit_H10->setText(list.at(9));
    else ui->lineEdit_H10->setText(tr(""));
    if(list.at(10) != "-") ui->lineEdit_H11->setText(list.at(10));
    else ui->lineEdit_H11->setText(tr(""));
    if(list.at(11) != "-") ui->lineEdit_H12->setText(list.at(11));
    else ui->lineEdit_H12->setText(tr(""));

    if(list.at(12) != "-") ui->lineEdit_H13->setText(list.at(12));
    else ui->lineEdit_H13->setText(tr(""));
    if(list.at(13) != "-") ui->lineEdit_H14->setText(list.at(13));
    else ui->lineEdit_H14->setText(tr(""));
    if(list.at(14) != "-") ui->lineEdit_H15->setText(list.at(14));
    else ui->lineEdit_H15->setText(tr(""));
    if(list.at(15) != "-") ui->lineEdit_H16->setText(list.at(15));
    else ui->lineEdit_H16->setText(tr(""));
}
// 辅助函数 -- 设置选中的基准点
void Dialog::help_setBase(QString s){
    int index = s.toInt();   // index: 1-16
    last_baseValue = index - 1;

    if(index == 1) ui->checkBox_BaseValue1->setChecked(true);
    else if(index == 2) ui->checkBox_BaseValue2->setChecked(true);
    else if(index == 3) ui->checkBox_BaseValue3->setChecked(true);
    else if(index == 4) ui->checkBox_BaseValue4->setChecked(true);

    else if(index == 5) ui->checkBox_BaseValue5->setChecked(true);
    else if(index == 6) ui->checkBox_BaseValue6->setChecked(true);
    else if(index == 7) ui->checkBox_BaseValue7->setChecked(true);
    else if(index == 8) ui->checkBox_BaseValue8->setChecked(true);

    else if(index == 9) ui->checkBox_BaseValue9->setChecked(true);
    else if(index == 10) ui->checkBox_BaseValue10->setChecked(true);
    else if(index == 11) ui->checkBox_BaseValue11->setChecked(true);
    else if(index == 12) ui->checkBox_BaseValue12->setChecked(true);

    else if(index == 13) ui->checkBox_BaseValue13->setChecked(true);
    else if(index == 14) ui->checkBox_BaseValue14->setChecked(true);
    else if(index == 15) ui->checkBox_BaseValue15->setChecked(true);
    else if(index == 16) ui->checkBox_BaseValue16->setChecked(true);

}
// 辅助函数 -- 保存参数
bool Dialog::help_saveParam(){
    unsigned int tmp = 0;
    /*************** (1) 读取输入值 **********************************/
    // 1,选中基准点
    if(last_baseValue == -1) {
        QMessageBox::information(this, tr("采集分析软件"), tr("没有选择基准点!\n"));
        return false;
    }
    pMainWindow->baseValue = last_baseValue;

    // 2,读取设置的设备ID
    tmp = ui->lineEdit_DevID->text().toInt();
    if(tmp > 65536)
    {
        ui->label__Message->setText("设备ID必须在0 - 65536之间");
        ui->label__Message->setStyleSheet("color:red;");
        return false;
    }
    this->pDevIDH = (tmp>>8) & 0XFF;
    this->pDevIDL = tmp & 0XFF;
    qDebug()<<"(dialog.cpp)设备ID: "<<tmp<<endl;
    tmp = 0;

    // 3,读取项目名称
    if(this->ui->lineEdit_projectName->text() == ""){
         QMessageBox::information(this, tr("采集分析软件"), tr("项目名称不能为空!\n"));
         return false;
    }
    this->proName = this->ui->lineEdit_projectName->text();

    // 4,读取工程流水号
    if(this->ui->lineEdit_projectID->text() == ""){
         QMessageBox::information(this, tr("采集分析软件"), tr("工程流水号不能为空!\n"));
         return false;
    }
   // this->ProjectID = this->ui->lineEdit_projectID->text();
    this->proId = this->ui->lineEdit_projectID->text();

    // 5,读取服务器地址, 为空默认不设置
    if(this->ui->lineEdit_serverAddr->text() != ""){
        if(pMainWindow->connectType == CONNECT_SERIAL || pMainWindow->connectType == CONNECT_SOCKET){
            QString server = this->ui->lineEdit_serverAddr->text();
            qDebug()<<"(dialog.cpp)"<<QString("SetServer" + server + "\r\n");
            QByteArray message;
            message.append(QString("SetServer: " + server + "\r\n"));
            // 下发命令SetServer: 设置服务器.
            if(pMainWindow->connectType == CONNECT_SERIAL){
                pMainWindow->uart_thread->my_serialport->write(message);
                pMainWindow->uart_thread->timer->start(TIMEOUTTIME);
            }else if(pMainWindow->connectType == CONNECT_SOCKET){
                pMainWindow->tcpclient_thread->socket->write(message);
                pMainWindow->tcpclient_thread->timer->start(TIMEOUTTIME);
            }
        }
    }

    // 6,读取设置的 采样频率
    this->pFre = ui->comboBox_SampleRte->currentText().toInt();

    // 7,读取设置的 服务器发送间隔
    this->pSendTimeServer = ui->comboBox_SendTime2erver->currentText().toInt();

    // 8,读取设置的 触发态发送间隔
    this->pSendTimeDynamic = ui->comboBox_SendTimeDynamic->currentText().toInt();

    // 9,读取设置的 数据自动保存间隔
    QString tmpPeriod = ui->comboBox_SaveDataDynamic->currentText();
    if(tmpPeriod == "手动保存"){
        pMainWindow->AutoSavePeriod = 0;
        pMainWindow->timer4AutoSaveFile->stop();
    }
    else{
        pMainWindow->AutoSavePeriod = ui->comboBox_SaveDataDynamic->currentText().toInt();
        pMainWindow->timer4AutoSaveFile->start(pMainWindow->AutoSavePeriod*1000);
    }

    // 10,读取设置的 数据保存路径
    if(ui->lineEdit_file4save->text() == ""){
        QMessageBox::information(this, tr("采集分析软件"), tr("数据保存路径不能为空!\n"));
        return false;
    }
    pMainWindow->AutoSaveFile = ui->lineEdit_file4save->text();

    // 11,读取设置的密度
    if(ui->lineEdit_Density->text() == ""){
        QMessageBox::information(this, tr("采集分析软件"), tr("密度设置不能为空!\n"));
        return false;
    }
    pMainWindow->Density = ui->lineEdit_Density->text().toDouble();

    // 12,读取设置的标定系数
    if(ui->lineEdit_biaoding->text() == ""){
        QMessageBox::information(this, tr("采集分析软件"), tr("标定系数设置不能为空!\n"));
        return false;
    }
    pMainWindow->e = ui->lineEdit_biaoding->text().toDouble();

    // 13,读取设置的Pmax,Pmin
    if(ui->lineEdit_Pmax->text() == ""){
        QMessageBox::information(this, tr("采集分析软件"), tr("Pmax设置不能为空!\n"));
        return false;
    }
    pMainWindow->Pmax = ui->lineEdit_Pmax->text().toDouble();
    if(ui->lineEdit_Pmin->text() == ""){
        QMessageBox::information(this, tr("采集分析软件"), tr("Pmin设置不能为空!\n"));
        return false;
    }
    pMainWindow->Pmin = ui->lineEdit_Pmin->text().toDouble();

    // 14,保存设置的通道数,H初值
    if(!isSelected[0] && !isSelected[1] && !isSelected[2] && !isSelected[3]
       && !isSelected[4] && !isSelected[5] && !isSelected[6] && !isSelected[7]
       && !isSelected[8] && !isSelected[9] && !isSelected[10] && !isSelected[11]
       && !isSelected[12] && !isSelected[13] && !isSelected[14] && !isSelected[15])
    {
        QMessageBox::information(this,tr("采集仪分析软件"),tr("请勾选要保存的通道"));
        return false;
    }
    if(isSelected[0]) pMainWindow->H[0] = (ui->lineEdit_H1->text().toDouble());
    else pMainWindow->H[0] = 0;
    if(isSelected[1]) pMainWindow->H[1] = ( ui->lineEdit_H2->text().toDouble());
    else pMainWindow->H[1] = 0;
    if(isSelected[2]) pMainWindow->H[2] = (ui->lineEdit_H3->text().toDouble());
    else pMainWindow->H[2] = 0;
    if(isSelected[3]) pMainWindow->H[3] = ( ui->lineEdit_H4->text().toDouble());
    else pMainWindow->H[3] = 0;
    if(isSelected[4]) pMainWindow->H[4] = ( ui->lineEdit_H5->text().toDouble());
    else pMainWindow->H[4] = 0;
    if(isSelected[5]) pMainWindow->H[5] = ( ui->lineEdit_H6->text().toDouble());
    else pMainWindow->H[5] = 0;
    if(isSelected[6]) pMainWindow->H[6] = ( ui->lineEdit_H7->text().toDouble());
    else pMainWindow->H[6] = 0;
    if(isSelected[7]) pMainWindow->H[7] = ( ui->lineEdit_H8->text().toDouble());
    else pMainWindow->H[7] = 0;
    if(isSelected[8]) pMainWindow->H[8] = ( ui->lineEdit_H9->text().toDouble());
    else pMainWindow->H[8] = 0;
    if(isSelected[9]) pMainWindow->H[9] = ( ui->lineEdit_H10->text().toDouble());
    else pMainWindow->H[9] = 0;
    if(isSelected[10]) pMainWindow->H[10] = ( ui->lineEdit_H11->text().toDouble());
    else pMainWindow->H[10] = 0;
    if(isSelected[11]) pMainWindow->H[11] = ( ui->lineEdit_H12->text().toDouble());
    else pMainWindow->H[11] = 0;
    if(isSelected[12]) pMainWindow->H[12] = ( ui->lineEdit_H13->text().toDouble());
    else pMainWindow->H[12] = 0;
    if(isSelected[13]) pMainWindow->H[13] = ( ui->lineEdit_H14->text().toDouble());
    else pMainWindow->H[13] = 0;
    if(isSelected[14]) pMainWindow->H[14] = ( ui->lineEdit_H15->text().toDouble());
    else pMainWindow->H[14] = 0;
    if(isSelected[15]) pMainWindow->H[15] = ( ui->lineEdit_H16->text().toDouble());
    else pMainWindow->H[15] = 0;

    // 15,读取设置的测试点名称  --> (未勾选的不保存)
    QString slist[16] ;
    if(isSelected[0]) slist[0] = ui->lineEdit_test1->text();
    else slist[0] = "";
    if(isSelected[1]) slist[1] = ui->lineEdit_test2->text();
    else slist[1] = "";
    if(isSelected[2]) slist[2] = ui->lineEdit_test3->text();
    else slist[2] = "";
    if(isSelected[3]) slist[3] = ui->lineEdit_test4->text();
    else slist[3] = "";
    if(isSelected[4]) slist[4] = ui->lineEdit_test5->text();
    else slist[4] = "";
    if(isSelected[5]) slist[5] = ui->lineEdit_test6->text();
    else slist[5] = "";
    if(isSelected[6]) slist[6] = ui->lineEdit_test7->text();
    else slist[6] = "";
    if(isSelected[7]) slist[7] = ui->lineEdit_test8->text();
    else slist[7] = "";
    if(isSelected[8]) slist[8] = ui->lineEdit_test9->text();
    else slist[8] = "";
    if(isSelected[9]) slist[9] = ui->lineEdit_test10->text();
    else slist[9] = "";
    if(isSelected[10]) slist[10] = ui->lineEdit_test11->text();
    else slist[10] = "";
    if(isSelected[11]) slist[11] = ui->lineEdit_test12->text();
    else slist[11] = "";
    if(isSelected[12]) slist[12] = ui->lineEdit_test13->text();
    else slist[12] = "";
    if(isSelected[13]) slist[13] = ui->lineEdit_test14->text();
    else slist[13] = "";
    if(isSelected[14]) slist[14] = ui->lineEdit_test15->text();
    else slist[14] = "";
    if(isSelected[15]) slist[15] = ui->lineEdit_test16->text();
    else slist[15] = "";

    // 16,读取设置的采样类型
    if(ui->comboBox->currentText() == "电压"){
        tmp |= 1<<0;
    }
    if(ui->comboBox_2->currentText() == "电压"){
        tmp |= 1<<1;
    }
    if(ui->comboBox_3->currentText() == "电压"){
        tmp |= 1<<2;
    }
    if(ui->comboBox_4->currentText() == "电压"){
        tmp |= 1<<3;
    }
    if(ui->comboBox_5->currentText() == "电压"){
        tmp |= 1<<4;
    }
    if(ui->comboBox_6->currentText() == "电压"){
        tmp |= 1<<5;
    }
    if(ui->comboBox_7->currentText() == "电压"){
        tmp |= 1<<6;
    }
    if(ui->comboBox_8->currentText() == "电压"){
        tmp |= 1<<7;
    }
    if(ui->comboBox_9->currentText() == "电压"){
        tmp |= 1<<8;
    }
    if(ui->comboBox_10->currentText() == "电压"){
        tmp |= 1<<9;
    }
    if(ui->comboBox_11->currentText() == "电压"){
        tmp |= 1<<10;
    }
    if(ui->comboBox_12->currentText() == "电压"){
        tmp |= 1<<11;
    }
    if(ui->comboBox_13->currentText() == "电压"){
        tmp |= 1<<12;
    }
    if(ui->comboBox_14->currentText() == "电压"){
        tmp |= 1<<13;
    }
    if(ui->comboBox_15->currentText() == "电压"){
        tmp |= 1<<14;
    }
    if(ui->comboBox_16->currentText() == "电压"){
        tmp |= 1<<15;
    }
    this->pCtrlH = (tmp>>8) & 0XFF;
    this->pCtrlL = tmp & 0XFF;

    /************* (2) 下传设置的命令字 ****************************/
    QByteArray message;
    message.append(QString("SetParam"));
    message.append(pDevIDH);
    message.append(pDevIDL);                 // 1,设备号
    message.append(pCtrlH);
    message.append(pCtrlL);                  // 2,采样类型
    message.append(pFre);                    // 3,采样频率
    message.append(pSendTimeServer);         // 4,服务器发送间隔
    message.append(pSendTimeDynamic);        // 5,触发态保存间隔
    /*
    // 1,写入工程流水号
    QString testProId = tr("Iam中00000!!");
    int testProId_Len = testProId.toUtf8().length();
    qDebug()<<"流水号长度: "<<testProId_Len<<";内容是: "<<testProId.toUtf8()<<";长度是: "<<testProId.toUtf8().length()<<endl;
    message.append(testProId);
    // 填0
    unsigned char fillCode = 0x00;   // 填充码
    for(int i = testProId_Len;i<48;i++){
        message.append(fillCode);
    }
    // 2,写入工程名称
    QString testProName = tr("Iam中00000!!");
    int testProName_Len = testProName.toUtf8().length();
    qDebug()<<"工程名称长度: "<<testProName_Len<<";内容是: "<<testProName.toUtf8()<<";长度是: "<<testProId.toUtf8().length()<<endl;
    message.append(testProName);
    // 填0
    for(int i = testProName_Len;i<48;i++){
        message.append(fillCode);
    }
    */
    int len = proId.toUtf8().length();
    unsigned char fillCode = 0x00;   // 填充码
    message.append(proId);                   // 6,工程流水号
    for(int i = len;i<48;i++){
        message.append(fillCode);
    }

    len = proName.toUtf8().length();
    message.append(proName);                 // 7,项目名称
    for(int i = len;i<48;i++){
        message.append(fillCode);
    }

    for(int j = 0;j<16;j++){                 // 8,测试点的名称
        len = slist[j].toUtf8().length();
        message.append(slist[j]);
        for(int i = len;i<24;i++){
            message.append(fillCode);
        }
    }

    message.append(QString("\r\n"));
    pMainWindow->tcpclient_thread->socket->write(message);
    pMainWindow->samplerateTmp = pFre;
    pMainWindow->tcpclient_thread->timer->start(TIMEOUTTIME);

    /************* (3) 保存用户设置于setting.csv文件中 ***********************/
    QFile mfile("./Setting.csv");
    if(!mfile.isOpen()) mfile.open( QIODevice::ReadWrite | QIODevice::Truncate |QIODevice::Text);
    QTextStream steam(&mfile);
    int devid = 0;
    devid = pDevIDH;
    devid <<= 8;
    devid += pDevIDL;
    steam<<devid<<endl;                      // 1,写入设备ID   (用于区分不同设备)
    steam<<tmpPeriod<<endl;                  // 2,写入自动保存间隔 或 手动保存
    steam<<pMainWindow->AutoSaveFile<<endl;  // 3,写入自动保存路径
    steam<<pMainWindow->Density<<endl;       // 4,写入密度
    steam<<pMainWindow->e<<endl;             // 5,写入标定系数e
    steam<<pMainWindow->Pmax<<endl;          // 6,写入Pmax
    steam<<pMainWindow->Pmin<<endl;          // 7,写入Pmin
    for(int i = 0; i < 16; i++){             // 8,写入选中的通道数
        if(isSelected[i])
            steam<<"1,";
        else
            steam<<"0,";
    }
    steam<<endl;
    for(int i = 0; i < 16; i++){             // 9,写入H初值
        if(isSelected[i])
            steam<<pMainWindow->H[i]<<",";
        else
            steam<<"-,";
    }
    steam<<endl;
    steam<<last_baseValue+1<<endl;            // 10,写入基准点选择
    pMainWindow->readInitialValue = true;
    cnt = 20;
    return true;
}

/************** 按钮响应函数 ********************************/
// "ok"         按钮: 保存参数到setting.csv, 下发设置到设备, 退出
void Dialog::accept()
{
    if(help_saveParam()) {
        pMainWindow->readInitialValue = false;
        Dialog::done(0);
    }
    else return;
}
// "cancel"     按钮:
void Dialog::reject()
{
    Dialog::done(-1);
}
// "浏览"        按钮: 选择文件自动保存路径
void Dialog::on_pushButton_selectFile_clicked()
{
   QFileDialog::Options options = QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly;
   QString fileName =  QFileDialog::getExistingDirectory(this,tr("保存目录"),
                                           this->ui->lineEdit_file4save->text(),
                                           options);
   if(!fileName.isEmpty()){
       this->ui->lineEdit_file4save->setText(fileName);
   }
}
// "恢复出厂设置" 按钮: 下发“恢复出厂设置”命令字
void Dialog::on_pushButton_Reset_2_clicked()
{
    if(pMainWindow->connectType == CONNECT_SERIAL || pMainWindow->connectType == CONNECT_SOCKET){
        bool isOK;
        QStringList list;
        list<<tr("恢复出厂设置")<<tr("点错了");
        QString select=QInputDialog::getItem(this,tr("采集分析软件"),tr("点击确定"),list,0,false,&isOK);
        if(select == "恢复出厂设置" && isOK){
            if(pMainWindow->connectType == CONNECT_SERIAL){
                // 下发命名字ResetOption:恢复出厂设置
                pMainWindow->uart_thread->my_serialport->write("ResetOption\r\n");
                pMainWindow->uart_thread->timer->start(TIMEOUTTIME);
            }else if(pMainWindow->connectType == CONNECT_SOCKET){
                pMainWindow->tcpclient_thread->socket->write("ResetOption\r\n");
                pMainWindow->tcpclient_thread->timer->start(TIMEOUTTIME);
            }
        }
    }
}
// "读取初值"    按钮: 打开计时器,下发“发送数据”命令字
void Dialog::on_pushButton_readH0_clicked()
{
   if(!isSelected[0] && !isSelected[1] && !isSelected[2] && !isSelected[3]
      && !isSelected[4] && !isSelected[5] && !isSelected[6] && !isSelected[7]
      && !isSelected[8] && !isSelected[9] && !isSelected[10] && !isSelected[11]
      && !isSelected[12] && !isSelected[13] && !isSelected[14] && !isSelected[15])
   {
       QMessageBox::information(this,tr("采集仪分析软件"),tr("请选择通道数"));
       return;
   }
   if(cnt>0){

      // 读取最新设置的参数值:密度,Pmax,Pmin,标记系数
      pMainWindow->e = ui->lineEdit_biaoding->text().toDouble();
      pMainWindow->Density = ui->lineEdit_Density->text().toDouble();
      pMainWindow->Pmax = ui->lineEdit_Pmax->text().toDouble();
      pMainWindow->Pmin = ui->lineEdit_Pmin->text().toDouble();

      QString str = "请等待" + QString::number(cnt) + "秒采集数据";
      msgBox->setInformativeText(str);
      if(!oneMinuteTimer->isActive()) oneMinuteTimer->start(1000);
      msgBox->setModal(true);
      msgBox->exec();
   }
}
void Dialog::handlerTimerOut(){         // 定时器处理函数
     if(cnt > 0){
          // (1)发送命令字
          if(cnt == 20 && pMainWindow->connectType == CONNECT_SOCKET) {
               // 读取密度
               pMainWindow->Density = ui->lineEdit_Density->text().toDouble();
               // 读取标定系数
               pMainWindow->e = ui->lineEdit_biaoding->text().toDouble();
               // 读取Pmax
               pMainWindow->Pmax = ui->lineEdit_Pmax->text().toDouble();
               // 读取Pmin
               pMainWindow->Pmin = ui->lineEdit_Pmin->text().toDouble();

               pMainWindow->tcpclient_thread->socket->write("StartToSend\r\n");
               qDebug()<<"(dialog.cpp)send to device: StartToSend\r\n"<<endl;
               pMainWindow->isStartCollect = true;
          }
          // (2) 等待
          cnt--;
          QString str = "请等待" + QString::number(cnt) + "秒采集数据";
          qDebug()<<"(dialog.cpp)"<<str<<endl;
          msgBox->setInformativeText(str);
     }
     else{
         msgBox->close();
         oneMinuteTimer->stop();
         pMainWindow->tcpclient_thread->socket->write("StopToSend\r\n");
         qDebug()<<"(dialog.cpp)send to device: StopToSend\r\n"<<endl;
         pMainWindow->isStartCollect = false;
         pMainWindow->readInitialValue = false;
         pMainWindow->timeCount = 0;

         // 显示读取到的H初始值
         if(pMainWindow->isChannal[0]) this->ui->lineEdit_H1->setText(QString::number(pMainWindow->H[0]));
         else  this->ui->lineEdit_H1->setText(tr(""));
         if(pMainWindow->isChannal[1]) this->ui->lineEdit_H2->setText(QString::number(pMainWindow->H[1]));
         else  this->ui->lineEdit_H2->setText(tr(""));
         if(pMainWindow->isChannal[2]) this->ui->lineEdit_H3->setText(QString::number(pMainWindow->H[2]));
         else  this->ui->lineEdit_H3->setText(tr(""));
         if(pMainWindow->isChannal[3]) this->ui->lineEdit_H4->setText(QString::number(pMainWindow->H[3]));
         else  this->ui->lineEdit_H4->setText(tr(""));
         if(pMainWindow->isChannal[4]) this->ui->lineEdit_H5->setText(QString::number(pMainWindow->H[4]));
         else  this->ui->lineEdit_H5->setText(tr(""));
         if(pMainWindow->isChannal[5]) this->ui->lineEdit_H6->setText(QString::number(pMainWindow->H[5]));
         else  this->ui->lineEdit_H6->setText(tr(""));
         if(pMainWindow->isChannal[6]) this->ui->lineEdit_H7->setText(QString::number(pMainWindow->H[6]));
         else  this->ui->lineEdit_H7->setText(tr(""));
         if(pMainWindow->isChannal[7]) this->ui->lineEdit_H8->setText(QString::number(pMainWindow->H[7]));
         else  this->ui->lineEdit_H8->setText(tr(""));

         if(pMainWindow->isChannal[8]) this->ui->lineEdit_H9->setText(QString::number(pMainWindow->H[8]));
         else  this->ui->lineEdit_H9->setText(tr(""));
         if(pMainWindow->isChannal[9]) this->ui->lineEdit_H10->setText(QString::number(pMainWindow->H[9]));
         else  this->ui->lineEdit_H10->setText(tr(""));
         if(pMainWindow->isChannal[10]) this->ui->lineEdit_H11->setText(QString::number(pMainWindow->H[10]));
         else  this->ui->lineEdit_H11->setText(tr(""));
         if(pMainWindow->isChannal[11]) this->ui->lineEdit_H12->setText(QString::number(pMainWindow->H[11]));
         else  this->ui->lineEdit_H12->setText(tr(""));
         if(pMainWindow->isChannal[12]) this->ui->lineEdit_H13->setText(QString::number(pMainWindow->H[12]));
         else  this->ui->lineEdit_H13->setText(tr(""));
         if(pMainWindow->isChannal[13]) this->ui->lineEdit_H14->setText(QString::number(pMainWindow->H[13]));
         else  this->ui->lineEdit_H14->setText(tr(""));
         if(pMainWindow->isChannal[14]) this->ui->lineEdit_H15->setText(QString::number(pMainWindow->H[14]));
         else  this->ui->lineEdit_H15->setText(tr(""));
         if(pMainWindow->isChannal[15]) this->ui->lineEdit_H16->setText(QString::number(pMainWindow->H[15]));
         else  this->ui->lineEdit_H16->setText(tr(""));
     }
}
// "保存参数"    按钮: 保存参数,下发设置到设备
void Dialog::on_pushButton_saveParam_clicked()
{
   help_saveParam();
}

/*************** 复选框响应函数 *******************************/
// 选择基准点
void Dialog::on_checkBox_BaseValue_clicked()
{
   //(1) 清除上次的标记
   if(last_baseValue != -1){
       switch(last_baseValue){
           case 0: ui->checkBox_BaseValue1->setChecked(false); break;
           case 1: ui->checkBox_BaseValue2->setChecked(false); break;
           case 2: ui->checkBox_BaseValue3->setChecked(false); break;
           case 3: ui->checkBox_BaseValue4->setChecked(false); break;
           case 4: ui->checkBox_BaseValue5->setChecked(false); break;
           case 5: ui->checkBox_BaseValue6->setChecked(false); break;
           case 6: ui->checkBox_BaseValue7->setChecked(false); break;
           case 7: ui->checkBox_BaseValue8->setChecked(false); break;

           case 8: ui->checkBox_BaseValue9->setChecked(false); break;
           case 9: ui->checkBox_BaseValue10->setChecked(false); break;
           case 10: ui->checkBox_BaseValue11->setChecked(false); break;
           case 11: ui->checkBox_BaseValue12->setChecked(false); break;
           case 12: ui->checkBox_BaseValue13->setChecked(false); break;
           case 13: ui->checkBox_BaseValue14->setChecked(false); break;
           case 14: ui->checkBox_BaseValue15->setChecked(false); break;
           case 15: ui->checkBox_BaseValue16->setChecked(false); break;

       }
   }
   //(2) 设置新的基准点
   if(ui->checkBox_BaseValue1->isChecked()) last_baseValue = 0;
   else if(ui->checkBox_BaseValue2->isChecked()) last_baseValue = 1;
   else if(ui->checkBox_BaseValue3->isChecked()) last_baseValue = 2;
   else if(ui->checkBox_BaseValue4->isChecked()) last_baseValue = 3;
   else if(ui->checkBox_BaseValue5->isChecked()) last_baseValue = 4;
   else if(ui->checkBox_BaseValue6->isChecked()) last_baseValue = 5;
   else if(ui->checkBox_BaseValue7->isChecked()) last_baseValue = 6;
   else if(ui->checkBox_BaseValue8->isChecked()) last_baseValue = 7;
   else if(ui->checkBox_BaseValue9->isChecked()) last_baseValue = 8;
   else if(ui->checkBox_BaseValue10->isChecked()) last_baseValue = 9;
   else if(ui->checkBox_BaseValue11->isChecked()) last_baseValue = 10;
   else if(ui->checkBox_BaseValue12->isChecked()) last_baseValue = 11;
   else if(ui->checkBox_BaseValue13->isChecked()) last_baseValue = 12;
   else if(ui->checkBox_BaseValue14->isChecked()) last_baseValue = 13;
   else if(ui->checkBox_BaseValue15->isChecked()) last_baseValue = 14;
   else if(ui->checkBox_BaseValue16->isChecked()) last_baseValue = 15;
   else last_baseValue = -1;

   qDebug()<<"(dialog.cpp)新选中的基准点是: "<<QString::number(last_baseValue)<<endl;
}
// 选择手动保存还是自动保存
void Dialog::on_comboBox_SaveDataDynamic_activated(const QString &str)
{
    // 如果是手动保存,则禁止选择自动保存路径,禁止点击“浏览按钮”
    if(str == "手动保存"){
        ui->pushButton_selectFile->setEnabled(false);
        ui->lineEdit_file4save->setEnabled(false);
    }else{
        ui->pushButton_selectFile->setEnabled(true);
        ui->lineEdit_file4save->setEnabled(true);
    }
}
// 选择通道
void Dialog::on_checkBox_ChannelSelected_clicked(){
    if(ui->checkBox_1->isChecked()) isSelected[0] = true;
    else isSelected[0] = false;
    if(ui->checkBox_2->isChecked()) isSelected[1] = true;
    else isSelected[1] = false;
    if(ui->checkBox_3->isChecked()) isSelected[2] = true;
    else isSelected[2] = false;
    if(ui->checkBox_4->isChecked()) isSelected[3] = true;
    else isSelected[3] = false;
    if(ui->checkBox_5->isChecked()) isSelected[4] = true;
    else isSelected[4] = false;
    if(ui->checkBox_6->isChecked()) isSelected[5] = true;
    else isSelected[5] = false;
    if(ui->checkBox_7->isChecked()) isSelected[6] = true;
    else isSelected[6] = false;
    if(ui->checkBox_8->isChecked()) isSelected[7] = true;
    else isSelected[7] = false;

    if(ui->checkBox_9->isChecked()) isSelected[8] = true;
    else isSelected[8] = false;
    if(ui->checkBox_10->isChecked()) isSelected[9] = true;
    else isSelected[9] = false;
    if(ui->checkBox_11->isChecked()) isSelected[10] = true;
    else isSelected[10] = false;
    if(ui->checkBox_12->isChecked()) isSelected[11] = true;
    else isSelected[11] = false;
    if(ui->checkBox_13->isChecked()) isSelected[12] = true;
    else isSelected[12] = false;
    if(ui->checkBox_14->isChecked()) isSelected[13] = true;
    else isSelected[13] = false;
    if(ui->checkBox_15->isChecked()) isSelected[14] = true;
    else isSelected[14] = false;
    if(ui->checkBox_16->isChecked()) isSelected[15] = true;
    else isSelected[15] = false;

}
