#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class Dialog;
}

class MainWindow;

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent, unsigned char devIDH, unsigned char devIDL, unsigned char ctrlH, unsigned char ctrlL,\
                    unsigned char fre, unsigned char sendTimeServer, unsigned char sendTimeDynamic,QString _proId,QString _proName, \
                    QString _testPointName[]);
    ~Dialog();
    MainWindow  *pMainWindow;
    bool isReject;
    unsigned char pDevIDH;
    unsigned char pDevIDL;              // deviceID设备ID
    unsigned char pCtrlH;
    unsigned char pCtrlL;               // 采样类型
    unsigned char pFre;                 // 采样频率
    unsigned char pSendTimeServer;      // 服务器发送间隔
    unsigned char pSendTimeDynamic;     // 触发态发送间隔

    QString ProjectID;                  // 工程流水号
    QString proId;        //  下传到设备的工程流水号
    QString proName;      //  下传到设备的项目名称
    QString testPointName[16];    //  测试点的名称

    char last_baseValue;       // 记录上一次的基准值
    QTimer *oneMinuteTimer;    // 定时1分钟采集数据
    QMessageBox* msgBox;       // 提示等待一分钟
    int cnt;                   // 计时60s
    bool isSelected[20];

private:
    Ui::Dialog *ui;
    void help_setChannel(bool a[]);      // 设置通道数
    void help_setTestName(QString s);    // 设置测试点名称
    void help_setH(QString s);           // 设置H初值
    void help_setBase(QString s);        // 设置基准点
    bool help_saveParam();               // 保存参数
public slots:
    virtual void accept();          // ok 按钮响应函数
    virtual void reject();          // cancel 按钮响应函数
    virtual void handlerTimerOut();
signals:
    void SendCTRL(int ctrl);         // 发送采样类型触发器

private slots:
    void on_pushButton_selectFile_clicked();
    void on_pushButton_Reset_2_clicked();
    void on_comboBox_SaveDataDynamic_activated(const QString &arg1);
    void on_pushButton_readH0_clicked();
    void on_checkBox_BaseValue_clicked();
    void on_checkBox_ChannelSelected_clicked();
    void on_pushButton_saveParam_clicked();
};

#endif // DIALOG_H
